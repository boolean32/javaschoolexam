package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.List;
import java.util.TreeSet;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers == null || inputNumbers.isEmpty() || inputNumbers.size() == 1) {
            throw new CannotBuildPyramidException();
        }
        TreeSet<Integer> uniqueElementSet = new TreeSet<>();
        inputNumbers.stream().map((num) -> {
            if (num == null) {
                throw new CannotBuildPyramidException();
            }
            return num;
        }).forEach(uniqueElementSet::add);
        int uniqueElementsAmount = uniqueElementSet.size();
        if (uniqueElementsAmount == 1) {
            throw new CannotBuildPyramidException();
        }
        int expectedNumberOfRows = 1;
        int requiredElements = 1;
        int currentRowElementNumber = 1;
        while (requiredElements < uniqueElementsAmount) {
            expectedNumberOfRows += 1;
            currentRowElementNumber += 1;
            requiredElements += currentRowElementNumber;
        }
        if (requiredElements != uniqueElementsAmount) {
            throw new CannotBuildPyramidException();
        }
        int rowSize = currentRowElementNumber * 2 - 1;
        int[][] pyramid = new int[expectedNumberOfRows][rowSize];
        for (int[] integers : pyramid) {
            Arrays.fill(integers, 0);
        }
        int skipRowElements = 0;
        int endingRowElement = rowSize - 1;
        for (int row = expectedNumberOfRows - 1; row >= 0; row--) {
            int last = endingRowElement - skipRowElements;
            int first = skipRowElements;
            for (int col = last; col >= first; col -= 2) {
                Integer value = uniqueElementSet.pollLast();
                if (value != null) {
                    pyramid[row][col] = value;
                }
            }
            skipRowElements += 1;
        }
        return pyramid;
    }


}
