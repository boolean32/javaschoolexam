package com.tsystems.javaschool.tasks.subsequence;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Queue;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }
        Queue queue = new LinkedList(x);
        Object searchElement = queue.peek();
        for (Object value : y) {
            if (Objects.equals(searchElement, value)) {
                queue.poll();
                if (queue.isEmpty()) {
                    break;
                }
                searchElement = queue.peek();
            }
        }
        return queue.isEmpty();
    }
}
