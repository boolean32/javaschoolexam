package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;

public class Expression {
    private final boolean closeable;
    private final Join join = new Join();
    private boolean closed = false;

    public Expression() {
        this(false);
    }

    public Expression(boolean closeable) {
        this.closeable = closeable;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public boolean isCloseable() {
        return closeable;
    }

    public Join getJoin() {
        return join;
    }

    public BigDecimal value() {
        if (closeable && !closed) {
            return null;
        }
        return join.evaluate();
    }
}
