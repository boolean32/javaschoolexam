package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;

public class Join {
    private Part first;
    private Part second;
    private Join next;
    private Operator op;

    public boolean operator(Operator op) {
        if (first != null && this.op == null) {
            this.op = op;
            return true;
        }
        return false;
    }

    public Join add(Num n) {
        return add(n, null);
    }

    public int priority() {
        return op == null ? -1 : op.getOrder();
    }

    private Join add(Num n, Expression e) {
        Join j;
        if (first != null && op == null) {
            return null;
        }
        if (first == null) {
            first = new Part(n, e);
            j = this;
        } else {
            second = new Part(n, e);
            next = new Join();
            next.add(n);
            j = next;
        }
        return j;
    }

    public Join add(Expression e) {
        return add(null, e);
    }

    public BigDecimal evaluate(BigDecimal ext) {
        BigDecimal current;
        if (second == null) {
            current = ext;
        } else if (next != null && next.priority() > priority()) {
            current = op.evaluate(ext, next.evaluate());
        } else {
            current = op.evaluate(ext, second.value());
            if (next != null) {
                current = next.evaluate(current);
            }
        }
        return current;
    }

    public BigDecimal evaluate() {
        BigDecimal current;
        if (op == null) {
            return first.value();
        } else if (next != null && next.priority() > priority()) {
            current = op.evaluate(first.value(), next.evaluate());
        } else {
            current = op.evaluate(first.value(), second.value());
            if (next != null) {
                current = next.evaluate(current);
            }
        }
        return current;
    }
}
