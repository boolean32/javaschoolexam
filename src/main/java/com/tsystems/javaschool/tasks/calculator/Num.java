package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.util.StringJoiner;

public class Num {

    private final StringBuilder integer = new StringBuilder();
    private final StringBuilder fractional = new StringBuilder();
    private boolean saveFraction = false;

    public void addDigit(char d) {
        if (saveFraction) {
            fractional.append(d);
        } else {
            integer.append(d);
        }
    }

    public void switchToFractional(int pos) {
        if (saveFraction) {
            throw new ReadingException('.', pos);
        }
        this.saveFraction = true;
    }

    public BigDecimal value() {
        StringJoiner sj = new StringJoiner(".");
        sj.add(integer).add(fractional);
        return new BigDecimal(sj.toString());
    }

    public boolean isDecimal() {
        return fractional.length() > 0;
    }
}
