package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;

public class Part {
    private final Num number;
    private final Expression exp;

    public Part(Num number, Expression exp) {
        this.number = number;
        this.exp = exp;
    }

    public BigDecimal value() {
        if (number != null) {
            return number.value();
        } else if (exp != null) {
            return exp.value();
        }
        return null;
    }
}
