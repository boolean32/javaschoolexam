package com.tsystems.javaschool.tasks.calculator;

public class ReadingException extends RuntimeException {

    public ReadingException(char symbol, int pos) {
        super("Illegal Symbol '" + symbol + "' at position " + pos);
    }
}
