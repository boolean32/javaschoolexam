package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null || statement.isEmpty()) {
            return null;
        }
        Expression ex = new Expression();
        try {
            read(statement, ex);
            BigDecimal value = ex.value();
            if (value != null) {
                return value.setScale(4, RoundingMode.DOWN)
                        .stripTrailingZeros().toPlainString();
            }
        } catch (Exception e) {
        }
        return null;
    }

    public int read(String text, Expression e) {
        return read(text, e, 0);
    }

    public int read(String text, Expression e, int start) {
        int last = text.length();
        Num n = null;
        Join j = e.getJoin();
        for (int i = start; i < last; i++) {
            char c = text.charAt(i);
            switch (c) {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    if (n == null) {
                        n = new Num();
                        j = j.add(n);
                        if (j == null) {
                            throw new ReadingException(c, i);
                        }
                    }
                    n.addDigit(c);
                    break;
                case '(':
                    e = new Expression(true);
                    j = j.add(e);
                    if (j == null) {
                        throw new ReadingException(c, i);
                    }
                    i = read(text, e, i + 1);
                    break;
                case ')':
                    if (e.isCloseable()) {
                        e.setClosed(true);
                        return i;
                    } else {
                        throw new ReadingException(c, i);
                    }
                case '+':
                case '-':
                case '/':
                case '*':
                    Operator op = Operator.fromValue(c);
                    if (!j.operator(op)) {
                        throw new ReadingException(c, i);
                    }
                    n = null;
                    break;
                case '.':
                    if (n != null) {
                        n.switchToFractional(i);
                    } else {
                        throw new ReadingException(c, i);
                    }
                    break;
                default:
                    throw new ReadingException(c, i);
            }
        }
        return text.length();
    }
}
