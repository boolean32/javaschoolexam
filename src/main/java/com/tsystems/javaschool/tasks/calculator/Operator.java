package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;

public enum Operator {
    PLUS(0, '+') {
        @Override
        public BigDecimal calculate(BigDecimal n1, BigDecimal n2) {
            return n1.add(n2);
        }

    },
    MINUS(0, '-') {
        @Override
        public BigDecimal calculate(BigDecimal n1, BigDecimal n2) {
            return n1.subtract(n2);
        }

    },
    DIVIDE(1, '/') {
        @Override
        public BigDecimal calculate(BigDecimal n1, BigDecimal n2) {
            return n1.divide(n2, 6, RoundingMode.UP);
        }

    },
    MULTIPLY(1, '*') {
        @Override
        public BigDecimal calculate(BigDecimal n1, BigDecimal n2) {
            return n1.multiply(n2);
        }

    };
    private final int order;
    private final char value;

    private Operator(int order, char value) {
        this.order = order;
        this.value = value;
    }

    public static Operator fromValue(char c) {
        for (Operator v : values()) {
            if (v.value == c) {
                return v;
            }
        }
        return null;
    }

    public int getOrder() {
        return order;
    }

    public abstract BigDecimal calculate(BigDecimal n1, BigDecimal n2);

    public BigDecimal evaluate(BigDecimal n1, BigDecimal n2) {
        if (n1 == null || n2 == null) {
            return null;
        }
        return calculate(n1, n2);
    }
}
